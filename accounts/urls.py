from django.urls import path
from accounts.views import Login, user_logout, sign_up

urlpatterns = [
    path("login/", Login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", sign_up, name="signup"),
]
